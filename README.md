## Virtualized Network zamanski.me
This project is an attempt to build a network from the ground up using a base virtual machine and configuration management tools.  This effort stems from multiple complementary goals.
- Learn to deploy and configure the core services necessary to run an enterprise network.
- Test drive configuration management tools.
- Learn to automate virtual machine deployments.
- Create a network environment to host various pet projects.

#### Network Components
- Firewalls (fw1,fw2): A firewall cluster consisting of two VMs running iptables, keepalived, and conntrackd to provide a high availability stateful firewall with automatic failover.  The firewall segregates 4 zones
  - External: Connection to the Internet (via my home network/router)
  - DMZ: External facing servers including web servers and external DNS
  - Linux: Internal servers such as database and hadoop servers
  - Windows: A Windows Active Directory domain
- Management (mgmt1): A jump server accessible from the outside world via SSH to the VIP of the external firewall zone.  The management server has a copy of this git repository and be used primarily to access and configure other servers.

#### Server Deployment & Configuration Example
This project is set up such that servers can be deployed and configured using a few quick scripts.  VirtualBox is used to deploy virtual machines, and Ansible is used for configuration management.
- Run: fw1.bat (on the VirtualBox host)
- Run: ansible-playbook firewalls.yml -i inventory -l fw1 -e ansible_ssh_host=192.168.1.202 -u root -k

#### CentOS Base VM Setup
- Create a new VM with a bridged network adapter
- Install CentOS from ISO
- Run: vi /etc/udev/rules.d/70-persistent-net.rules
    remove everything below header comment
- Run: vi /etc/sysconfig/network-scripts/ifcfg-eth0
    replace with
        DEVICE=eth0
        TYPE=Ethernet
        ONBOOT=yes
        BOOTPROTO=dhcp
- Run: shutdown -h now
- Take a snapshot of the VM