import os, yaml

group_vars = yaml.load(open("../group_vars/all", 'r'))
host_vars = {key: yaml.load(open("../host_vars/{}".format(key), 'r')) for key in os.listdir('../host_vars')}

for hostname, data in host_vars.items():
  filename = "{}.bat".format(hostname)
  with open(filename, 'w') as script:
    
    # Clone base VM
    script.write('VBoxManage clonevm "{image}" --snapshot "{snap}" --basefolder "{dir}" --name "{name}" --options link --register\n'.format(
      image=group_vars['image_name'], snap=group_vars['image_snapshot'], dir=group_vars['vm_directory'], name=hostname))
    # Assign VM to groups
    script.write('VBoxManage modifyvm "{name}" --groups "{groups}"\n'.format(name=hostname, groups=data['group']))
    
    # Configure network interfaces
    for ifname, ifdata in sorted(data['nic'].items(), key=lambda (k,v): v['num']):
      adapters = {'bridged': 'bridgeadapter', 'intnet': 'intnet'}
      if 'type0' in ifdata:
        type_val = ifdata['type0']
        seg_val = ifdata['seg0']
      else:
        type_val = ifdata['type']
        seg_val = ifdata['seg']
      net_config = '--nic{num} {type} --{adapter}{num} "{seg}"'.format(num=ifdata['num'], type=type_val, adapter=adapters[type_val], seg=seg_val)
      script.write('VBoxManage modifyvm "{name}" --macaddress{num} {mac} {net_config}\n'.format(
        name=hostname, num=ifdata['num'], mac=ifdata['mac'], net_config=net_config))
    
    # Start VM
    script.write('VBoxManage startvm "{name}"\n'.format(name=hostname))

    # Reconfigure network interfaces
    instructions_printed = False
    for ifname, ifdata in sorted(data['nic'].items(), key=lambda (k,v): v['num']):
      
      # Check if interface needs reconfiguration
      if 'type0' in ifdata:
        
        # Print instructions
        if not instructions_printed:
          script.write("echo off\n")
          script.write("echo 1) Determine the host's IP\n")
          script.write("echo 2) Run 'ansible-playbook PLAYBOOK.yml -i inventory -l HOSTNAME -e ansible_ssh_host=IP_ADDRESS -u root -k'\n")
          script.write("echo 3) Wait for the server to shutdown\n")
          script.write("pause\n")
          script.write("echo on\n")
          instructions_printed = True
        
        # Reconfigure interface
        script.write('VBoxManage modifyvm "{name}" --nic{num} {type} --{adapter}{num} "{seg}"\n'.format(
            name=hostname, num=ifdata['num'], type=ifdata['type'], adapter=adapters[ifdata['type']], seg=ifdata['seg']))
      
    # Start VM
    if instructions_printed:
      script.write('VBoxManage startvm "{name}"\n'.format(name=hostname))
      script.write("echo off\n")
      script.write("pause\n")
